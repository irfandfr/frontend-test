/*
This app is a test for a frontend developer position

This app fetch a transactional data from an external url and the user
  can search for a specific beneficiary name within that set of data

Code with <3 By Irfandhia Firdaus R.
*/

import React, { Component } from 'react'
import './App.css';
import TransactionList from './Component/TransactionList';
import SearchBar from './Component/SearchBar';

class App extends Component{
  state = {
    list : [],
    match : ''
  }
  /**
   * fetch data, add cors-anywhere to allow cross origin resource sharing
   */
  componentDidMount(){
      fetch('https://cors-anywhere.herokuapp.com/https://nextar.flip.id/frontend-test')
        .then(response => response.json())
        .then(result => this.setState({
            list : Object.values(result)
        }))
  }
  /**
   * update user filter
   */
  filterHandle = (filter) => {
    this.setState({
      match: filter
    })
  }
  render(){
    return(
      <div className='app'>
        <div className="orangeHeader"></div>
        <SearchBar match={this.state.match} filterChangeHandle={this.filterHandle}/>
        <TransactionList list={this.state.list} match={this.state.match}/>
      </div>
    )
  }
}

export default App;
