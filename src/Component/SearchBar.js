/**
 * this component accepts users input and pass it on to its parent
 *  the inputs are then used to filter the transactional data
 * 
 * Code with <3 by Irfandhia Firdaus R. 
 */

import React from 'react'

const SearchBar = (props) => {
    /**
     * handle text input whenever the value changes by passing onto parent
     */
    const onChangeHandle = (event) =>{
        props.filterChangeHandle(event.target.value);
    }
    return(
        <div className='search-bar'>
            <div className='search-bar--container'>
                <i className='search-bar--icon'></i>
                <input className='search-bar--input' type="text" name="filter" onChange={onChangeHandle} value={props.match}/>
            </div>
        </div>
    )
}

export default SearchBar;