/**
 * this component loops through the transaction list passed on by the parent
 * and map each filtered transaction to another component
 * 
 * Code with <3 by Irfandhia Firdaus R. 
 */
import React, { Component } from 'react'
import Transaction from './Transaction'

class TransactionList extends Component {
    render(){
        if (this.props.list.length > 0){
            return(
                <div className="transaction-container">
                    {this.props.list.map((transaction => {
                        if((transaction.beneficiary_name.match(new RegExp(this.props.match,'gi'))) !== null){
                            return <Transaction transaction={transaction} key={transaction.unique_code}/>
                        }else{
                            return ''
                        }   
                    }))}
                </div>
            )
        }else {
            return(
                <div className="transaction-container text--center">
                    <p>No available transaction to display...</p>
                </div>
            )
        }
    }
}

export default TransactionList