import React from 'react'

const Transaction = (props) => {
    const getDateText = (date) => {
        let year = date.substring(0,4);
        let day = date.substring(8,10);
        let month;
        switch (date.substring(5,7)){
            case '1':
                month = 'Januari';
                break;
            case '2':
                month = 'Februari';
                break
            case '3':
                month = 'Maret';
                break;
            case '4':
                month = 'April';
                break
            case '5':
                month = 'Mei';
                break;
            case '6':
                month = 'Juni';
                break
            case '7':
                month = 'Juli';
                break;
            case '8':
                month = 'Agustus';
                break
            case '9':
                month = 'September';
                break;
            case '10':
                month = 'Oktober';
                break
            case '11':
                month = 'November';
                break;
            case '12':
                month = 'Desember';
                break
            default:
                month = '';
                break;
        }
        return day+ " " + month + " " + year
    }
    /**
     * Convert transaction status to presentable format
     * (success => Berhasil, else => Pending)
     */
    const getTransactionStatus = (status) => {
        if(status.toLowerCase() === 'success'){
            return 'Berhasil';
        }else {
            return 'Pending';
        }
    }
    /**
     * function to convert number to readable number by adding '.'(dots)
     *  at three digits intervals
     *  (ex: 1200 -> 1.200)
     */
    const numberShowDot = (number) => {
        number = number + '';
        let digit = (number).length;
        let numComma = '';
        for(let i = 0 ; i < digit ; i = i + 3){
            if(i + 3 < digit){
                numComma = '.' + number.substring(digit - i - 3 ,digit - i) + numComma;
            } else {
                numComma = number.substring(digit - i - 3 ,digit - i) + numComma;
            }
        }
        return numComma
    }
    return(
        <div className={"transaction-item transaction-item_"+props.transaction.status.toLowerCase()} key={props.transaction.unique_code}>
            <div className='transaction-item--details'>
                <p><b>{props.transaction.sender_bank.toUpperCase()} ➔ {props.transaction.beneficiary_bank.toUpperCase()}</b></p>
                <p>{props.transaction.beneficiary_name.toUpperCase()}</p>
                <p>Rp{numberShowDot(props.transaction.amount)} &#8226; {getDateText(props.transaction.completed_at)}</p> 
            </div>
            <p className={'transaction-item--label ' + props.transaction.status.toLowerCase()}><b>{getTransactionStatus(props.transaction.status)}</b></p>
        </div>  
    )               
}

export default Transaction;